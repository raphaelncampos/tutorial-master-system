# Tutoriais do Sega Master System no Linux

A ideia deste tutorial é adaptar o conhecimento do tutorial do [Maxim](http://www.smspower.org/maxim/HowToProgram/Index) para desenvolvimento em plataforma Linux.

## Referências

[SMSPower!](http://www.smspower.org/)
[Maziac](https://github.com/maziac/)

## Documentações auxiliares

Recomendações pessoais do Maxim para desenvolvimento para o SMS:

- Charles MacDonald's [docs](http://cgfm2.emuviews.com/sms.php)
    1. [smstech-20021112.txt](http://www.smspower.org/maxim/uploads/HowToProgram/smstech-20021112.txt)
    2. [msvdp-20021112.txt](http://www.smspower.org/maxim/uploads/HowToProgram/msvdp-20021112.txt)
- Richard Talbot-Watkin's [doc](http://www.smspower.org/maxim/uploads/HowToProgram/richard.txt)
- [The Official Z80 User Manual](http://www.smspower.org/maxim/uploads/HowToProgram/z80cpu_um.zip)
- [WLA DX manual](http://www.villehelin.com/wla.txt)

## Programas

- Um editor de texto para seu assembly do Z80;
- Um assembler;
- um emulador com capacidades de debug;
- Outros
    1. Editor de imagens;
    2. Editor de hexadecimal;
    3. Conversor de gráficos;

### Editor

O Maxim utiliza o [ConTEXT](http://www.contexteditor.org/) com um [syntax highlighter](http://www.smspower.org/maxim/uploads/HowToProgram/contextwlaz80highlighter.zip) que ele fez. Vamos adaptar para nossa realidade no Ubuntu.

#### Opção 1

`gedit` com adição de _syntax hilighting_

Para adicionar o _syntax highlighting_ no gedit, fiz o [download](http://cemetech.net/scripts/countdown.php?/win/asm/z80.lang.zip&path=archives) do arquivo conforme este [tópico](https://www.cemetech.net/forum/viewtopic.php?t=5867&start=0)

> gedit ("Text Editor" in newer versions of GNOME) is a great tool for developers, featuring syntax highlighting, parenthesis matching, and automatic indentation among other things. It's great for all your coding needs, but it also has a conspicuous lack of syntax support for Z80 assembly programs. This gedit language file adds Z80-specific information for gedit users to enjoy.

*Nota:* também disponível na pasta _assets_ deste projeto.

para instalar:

```bash
mkdir -p /usr/share/gtksourceview-3.0/language-specs
sudo mv /path/to/z80.lang /usr/share/gtksourceview-3.0/language-specs
```

#### Opção 2

`vs-code` com plugins [?]

RGBDS Z80
> Name: RGBDS Z80
> Id: donaldhays.rgbds-z80
> Description: Language service for RGBDS GB Z80.
> Version: 2.4.1
> Publisher: Donald Hays
> VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=donaldhays.rgbds-z80

markdownlint
> Name: markdownlint
> Id: davidanson.vscode-markdownlint
> Description: Markdown linting and style checking for Visual Studio Code
> Version: 0.30.2
> Publisher: David Anson
> VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint

### Assembler

O Maxim sugere o [WLA DX](http://www.villehelin.com/wla.html). Segue o passo a passo para o por funcionando no Ubuntu:

```bash
git clone https://raphaelncampos@github.com/vhelin/wla-dx.git #baixar o projeto do github
sudo apt-get install build-essential cmake # precisamos do make e do cmake
cmake -G "Unix Makefiles"
make
sudo make install
```

isto deve bastar para ter os programas do WLA DX no seu `/usr/local/bin`.

### Emulador

O emulador escolhido é o [MEKA](http://www.smspower.org/meka/). Para instalá-lo no Ubuntu:

```bash
git clone https://raphaelncampos@github.com/ocornut/meka.git #clonar o projeto no github
#git submodule update --init --recursive #eu não fiz isso
sudo apt-get install liballegro5.2 nasm liballegro5-dev
```

em `meka/srcs/Makefile`, eu troquei:

*LIB_ALLEG = \`pkg-config --cflags --libs allegro-5.0 allegro_image-5.0 allegro_audio-5.0 allegro_font-5.0 allegro_primitives-5.0 allegro_ttf-5.0\`*

por

*LIB_ALLEG = \`pkg-config --cflags --libs allegro-5 allegro_image-5 allegro_audio-5 allegro_font-5 allegro_primitives-5 allegro_ttf-5\`*

```bash
cd meka/srcs
make
```

![Debugger Screenshot](http://www.smspower.org/forums/files/meka_080_wip_debugger_823.png)

![Debugger Screenshot](http://www.smspower.org/meka/gallery/meka072-wip-sagaia.png)

### Outros

#### Editor de imagens

O Maxim usa o `Paint Shop Pro 7` para edição de imagens, mas ainda não encontrei algo que o substitua (TO DO: encontrar uma maneira de compatibilizar o perfil de cor)

##### GIMP 2.0

Podemos incluir a paleta de cores do master system no GIMP, e utilizar este programa.

```GIMP
Window > Dockable Dialogs > Palletes
```

```GIMP
(Right Click in Palletes) > Import Pallete...
```

```GIMP
From File > HW-6-bit-RGB.gpl > Import
```

To Save:

```GIMP
Image > Mode > Indexed..
```

```GIMP
Colormap: Use custom pallete.....
```

```GIMP
Export > file.bmp
```

##### Aseprite

Um editor que pode ser muito útil é o [Aseprite](https://github.com/aseprite/aseprite).

###### Dependências:


* The latest version of [CMake](https://cmake.org) (3.14 or greater)
* [Ninja](https://ninja-build.org) build system
* You will need a compiled version of the Skia library.
  Please check the details about [how to build Skia](#building-skia-dependency)
  on your platform.

APT:
```bash
sudo apt-get install -y g++ cmake ninja-build libx11-dev libxcursor-dev libgl1-mesa-dev libfontconfig1-dev
```

Ninja:
```bash
cd ~/git/pessoal
git clone git://github.com/ninja-build/ninja.git && cd ninja
git checkout release
cat README
```

Skia:
```bash
#mkdir $HOME/deps
#cd $HOME/deps
cd ~/git/pessoal
git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git
git clone -b aseprite-m71 https://github.com/aseprite/skia.git
export PATH="${PWD}/depot_tools:${PATH}"
cd skia
python tools/git-sync-deps
gn gen out/Release --args="is_debug=false is_official_build=true skia_use_system_expat=false skia_use_system_icu=false skia_use_system_libjpeg_turbo=false skia_use_system_libpng=false skia_use_system_libwebp=false skia_use_system_zlib=false"
ninja -C out/Release skia
```

Aseprite:
```bash
cd ~/git/pessoal
git clone --recursive https://github.com/aseprite/aseprite.git
```

Make:
```bash
cd aseprite
mkdir build
cd build
cmake \
  -DCMAKE_BUILD_TYPE=RelWithDebInfo \
  -DLAF_OS_BACKEND=skia \
  -DSKIA_DIR=$HOME/git/pessoal/skia \   
  -DSKIA_OUT_DIR=$HOME/git/pessoal/skia/out/Release \
  -G Ninja \
  ..
ninja aseprite
```


#### Editor hexadecimal

O maxim sugere o [frhed](http://frhed.sourceforge.net/) para edição de hexadecimal, mas ainda não procurei nada sobre.

#### Conversor de gráficos

A sugestão para conversão de gráficos é o programa [BMP2Tile](http://www.smspower.org/maxim/Software/BMP2Tile). Ainda não o pus para funcionar no Ubuntu. Até agora, os passos seguidos são:

```bash
git clone https://raphaelncampos@github.com/maxim-zhao/bmp2tile.git #clonando o projeto
cd bmp2tile
```

O projeto foi escrito em .NET, portanto, segundo o [Gabs Ferreira](http://gabsferreira.com/instalando-e-usando-o-net-core-no-linux/), segue os passos para instalar o .NET Core:

```bash
wget -q https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb  
sudo add-apt-repository universe  
sudo apt-get install apt-transport-https  
sudo apt-get update  
sudo apt-get install dotnet-sdk-2.2
dotnet --version
```

Instalando dependências para o System.Drawing.Imaging:

```bash
sudo apt install libc6-dev libgdiplus
```

```bash
 dotnet add package System.Drawing.Common #instala o pacote no .NET Core
```

[...]

Conclusões: Muito complicado compilar uma coisa que foi feita para windows. Melhor reescrever

## Rodando em hardware real

Maxim disse:
> I am lucky enough to have an SMSReader and devcart so I have the software for that too, of course. (Getting homebrew games working on real hardware can be a real trial...) But you won’t need them for a while yet.

## Rodando o arquivo

Se tudo deu certo até aqui, basta rodar:

```bash
rm object.o #remove quaisquer objetos anteriores
wla-z80 -o object.o hello-world-2.asm #converte do assembly para object
echo "[objects]" > linkfile #inclui o cabeçalho de objects
echo object.o >> linkfile # inclui o conteúdo do objeto
wlalink -d -r -v -s linkfile hello-world-2.sms #compila para a ROM
path/to/meka hello-world-2.sms # executa a ROM no emulador
```
