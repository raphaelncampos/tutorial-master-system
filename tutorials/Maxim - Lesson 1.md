# Maxim - Lesson 1

Veja em `http://www.smspower.org/maxim/HowToProgram/Lesson1`

## Conceitos de assembly

A linguagem Assembly é específica para cada processador, neste caso vamos trabalhar com o Assembly do Z-80.
Num resumo simplista, é uma maneira de escrever os dados binários que o processador executa.

**Comentários**  são essenciais.

**Representação numérica**  vai apresentar os dados hexadecimais e binários.

**Diretivas** do assembler (WLA-DX) oferecem controle na maneira que o assembler entende o nosso código.
 
**Rótulos** nos ajudam a referenciar dados e código de maneira legível.

**Códigos de Operação** permitem total controle nas instruções do processador.

**Memória** é onde armazenamos o código, e de onde os dados vem, e para onde eles vão.

**Registradores** armazenam os dados que estamos trabalhando 

**Portas** são como nos comunicamos com o o mundo exterior.


## <a name="comment"></a>Comentários

### Comentários de linha

Tudo que vem depois de um `;` é ignorado até o fim da linha.

Ex.:
```asm
ld b,10 ; Inicializa o contador
```

### Comentários de bloco

O WLA-DX também aceita comentários multilinhas. Tudo após`/*` é ignoorado até encontrar `*/`.

Ex.:
```asm
/* Skip outputting
    ld a,$00
    out ($bf),a
    ld a,$38|$40
    out ($bf),a
    ld hl,Message
-:      ld a,(hl)
        cp $00
        jp z,+
        sub $20
        out ($be),a
        ld a,$00
        out ($be),a
        inc hl
        jp -
+:
*/
```

Ex.:
```asm
/****************************************
 In loving memory of Acorn Computers Ltd.
      Mike G proudly presents the:

       BBC Micro Character Set!
       ========================

 This source was created automatically
 from character data stored in the BBC
 Microcomputer operating system ROM.

 The BBC set is a nice, clear font which
 (unlike the classic Namco/Sega font)
 includes lower-case letters as well as
 upper-case.

 Being a British micro, there's even a
 Pound Sterling currency symbol, which
 should come in handy when I begin to
 write financial apps on the SMS. (Just
 kidding!)

 Hopefully by using binary representation
 it will be obvious how the character
 data is stored.

*****************************************/
```

## Números

### Binários

No nível mais baixo, em linguagem de máquina é tudo 0 ou 1, na base 2. Para expressar um binário no WLA-LX, use a sintaxe:

`%10000110`

É um número de 8 dígitos. O Z-80 trabalha com números binários de 8 dígitos, Por isso, o Z-80 é um processador de 8 bits.

 ### Hexadecimais
 
 O número binário é ideal para o computador, mas não é muito prático para os humanos. Portanto, podemos utilizar o 
 número hexadecimal (base 16). Sintaxe:
 
 `$a10c`

Alguns detalhes sobre os hexadecimais:

- $9 + 1 = $a 
- há algumas maneitas diferentes para representá-los:
    - $a10c 
    - 0xa10c 
    - &a10c
    - A10Ch 
- Dois hexadecimais são suficientes para representar um byte. Quando for preciso representar um byte, ter certea de 
adicionar um zero à esquerda quando necessário
- Quando o número representar uma *palavra* (16 bits), devemos ter certeza que temos 4 dígitos (ex.: $0002)

## Diretivas

 
Diretivas são os comandos para o *assembler* (no nosso caso, WLA DX). São comandos para dizer como e como fazer coisas, 
para quase tudo que não envolva o código-fonte em si.

É fácil identificar uma diretiva no código porque todas começam com um ponto na frente dos seus nomes. Estas estão bem 
documentadas na documentação do WLA DX.

A seguir, as diretivas mais úteis:

### Layout de memória

- `.org` e `.bank` informam ao WLA DX exatamente onde a saída deve ser posta no arquivo.
- `.memorymap` informa ao WLA DSX onde as memórias **RAM** e **ROM** aparecem do ponto de vista do processador.
- `.rombankmap` informa ao WLA DX como o arquivo da **ROM** é estruturado, em termos de mapeamento.
- `.section` e `.slot` nos permitem passar ao WLA DX as decisões de onde pôr código e dados, 
e como descartas partes não usadas.
- `.ramsection` nos permite estruturar o uso de memória de uma maneira flexível que resulte em símbolos de depuração.

### Definição de dados

- `.db`, `.dsb`, `.dw` e `.dsw` são itulizados para especificar dados binários (raw) no nosso código fonte.
- `dbsin`, `.dbcos` and `.dbrnd`(e seus equivalentes `.dwXXX`)permitem o WLA DX nos gerar alguns dados.
- `.struct` e `.dstruct` permitem trabalhar com dados estruturados (com múltiplas instâncias) de uma maneira limpa.
- `.enum` pode ser útil para gerar nomes para coisas como estados de objetos, de uma maneira similar a como são usados 
os `enums` em C/C++/C#/Java/etc. (Estes também podem ser utilizados para gerenciamento de memória, mas tem lá suas 
desvantagens).

### Auxiliares específicos de sistema

O WLA DX suporta vários processadores e consoles diferentes, e ele possui diretivas auxiliares paragarantir que o 
arquivo resultante é válido para o sistema em questão. Para o Master System, a principal que usaremos é a `.sdsctag`, 
que faz o seguinte:

- Isnsere todos os dados necessários para que o arquivo resultante passe nos testes da BIOS, de forma que o compilado 
rode num sistema com BIOS.
- Insere uma tag especial **SDSC** que permite incluir o título do programa, seu nome, a versão e um comentário 
arbitrário; Emuladores e ferramentas gerenciadoras de ROMs podem exibir isto.

### Inclusão condicional

 Se houver partes do seu código que você gostaria de excluir do resultado ocasionalmente - como se fingisse que o código 
 ou dado nunca estivese lá - você pode utilizar várias diretivas `.if`. Seu funcionamento é similar ao `#ifdef` do C/C++.
 
### Macros

Macros permitem inserir coisas no seu código baseado em parâmetros ou dados externos. As macros podem ser complicadas, 
mas são muito úteis às vezes. Será incluída num tutorial futuro. 

## <a name="label"></a<Rótulos

Rótulos são strings de texto (sem espaços ou pontuação) que permitem referenciar linhas de código ou dados por um nome, 
no lugar de um número.

Quando o código é montado, tudo é convertido para números. Isto significa que se você quiser referenciar algum dado 
gráfico ou uma rotina de código, por exemplo, em tempo de compilação, esta área será meramente um endereço de memória 
na ROM ou na RAM. Contudo, quando você está escrevendo seu código, não é desejável predefinirestes endereços; Você 
prefere que o *assembler* descubra onde pôr todas as seções de código, e depois preencha estes endereços de acordo.

Na prática, significa que em vez disso:

```asm
; carregar dados
ld hl,$100
call $200
...
.org $100
.incbin "dados.bin"
...
.org $200
; função de carregamento de dados
```

Você pode fazer isto:

```asm
; carregar dados
ld hl, AlgumDado
call CarrDado
...
AlgumDado:
.incbin "dados.bin"
... 
CarrDado:
; função de carregamento de dados
```

O *assembler* inclui todos os códigos e dados, junta tudo na saída (com espaços reservados para os endereços), e 
depois retorna e preenche os endereços utilizados. ós não precisamos nos lembrar o que está em qual endereço de memória.

## Códigos de Operação

Códigos de Operação (opcodes) são as instruções que serão executadas no Master System (ou Game Gear) pelo processador 
Z80. Elas são conjuntos de valores cuidadosamente estruturados para bytes, o que facilita o entendimento para o 
processador, mas é horrível para os humanos entenderem.

NO entanto, nós utilizamos programas assemblers que entendem **mnemônicos**. São pequenas "palavras" que representam o 
que os opcodes fazem. Para o Z80, são geralmente palavras em inglês (quase sempre abreviadas) que representam o que faz 
uma instrução, ou uma inicialização de uma frase que a descreve. Isto torna o código fonte mais legível. Exemplo:

```asm
ld bc, $4000    ; Carrega o par de registradores BC com o número $4000
out ($be),a     ; Direciona a saída para para a porta número $be o número armazenado no registrador A.
dec bc          ; Decrementa o par de registradores BC
ld a,b          ; Carrega o registrador A com o conteúdo do registrador B.
or c            ; Executa uma operação lógica OU entre os registradores A e C.
jp nz,UmRotulo  ; Pula, se o resultado é não-zero, para o endereço UmRotulo
```

Uma vez que você fique familiarizado com os mnemônicos, não será mais necessário adicionar este tipo de comentário ao 
código-fonte por toda parte.

Há mais de 1000 opcodes únicos possíveis, mas eles se dividem em 158 "tipos de instrução", onde a operação é na verdade 
a mesma, mas os registradores (operandos) são diferentes. E destas, nós usaremos principamente algumas dúzias, portanto 
não se impressione. As instruções cobrem os grupos:

- Aritmética de 8 bits e operações lógicas: tipos de operação entre Adição, Subtração, E, OU, OU EXCLUSIVO
- Aritmética de 16 bits: Similar à aritmética de 8 bits.
- Carregamento de 8 e 16 bits: movendo dados
- Definição, redefinição e teste de bits: Chiecando e manipulando 0s e 1s individuais.
- Chamada, retorno, e reinício: para funções.
- Troca, transferência de blocos e busca: estas simplificam operações que trabalham com muitos dados.
- Aritmética de propósito geral e controle de CPU: São auxiliares para manipulação de dados e controle de como 
funciona a CPU.
- Entrada e saída: A CPU pode se comunicar cim outros dispositivos no sistema através destes.
- Jump: Permite a execução de diferentes bits de código em diferentes condiçeõs.
- Rotação e deslocamento: Pode ser utilizado para cálculos, mas pode ser confuso a primeira vista.

O Manual do Usuário do Z80 agrupa as instruções desta forma.

Será descrito cada uma destas instruções conforme estas aparecem neste tutorial. Você não precisa memorizar nada :) 
apenas permita que elas naturalmente entrem no seu fluxo de trabalho, e procure por referência as que você esquecer.

## <a name="memory"></a>Memórias

Há (amplamente) dois tipos de memória - ROM e RAM.

### ROM

ROM (Read-only memory / Memória somente leitura) é a memória cheia de dados que você pode ler, mas não pode alterar. 
Isto é porque é o que estão dentro dos cartuchos, e é por isso que as pessoas chamam os dados despejados de "ROMs".

Sob a nossa perspectiva, ROM é onde residem nosso código e dados, pelo menos num início.

Nós podemos ter (quase) o quanto de memória a gente quiser, embora de fato seja difícil preencher mais do que algumas 
dúzias de KB.

### RAM

RAM (Random Access Memory / Memória de acesso aleatório) é uma memória inicialmente vazia, mas você pode fazer leitura 
e escrita de dados depois. Em geral, nós consideramos a RAM de dentro do console que é usada por cada jogo que você 
joga (embora *game saves* seja um tipo de RAM, a alguns jogos tenham RAM extra para suplementar a RAM do sistema).

Sob a nossa perspectiva, a RAM é onde nós mantemos quaisquer dados que tenhamos calculado ou definido no código, 
especialmente dados que precisem ser relembrados e/ou precisem de alguma alteração.

O significado de RAM e ROM não tem nada a ver um com o outro, apenas são visualmente similares. Acesso aleatório 
significa que você pode acessar qualquer parte desta memória sem ter de percorrê-la, mais ou menos como comparar um 
CD/DVD a uma fita cassete/VHS, mas isto também se aplica a memória ROM!

### Espelhamento

Por causa da maneira que os chips são conectados ao procesador, é possível conectar um chip de maneira que ele preencha 
um espaço **maio** do que a quantidade de memória no chip. Tamanhos de memória são geralmente potências de 2 (4KB, 8KB, 
16KB, etc), então o espaço será duas ou quatro vezes o tamanho da memória. O resultado, visto do ponto de vista do 
processador, é que toda a memória é repetida para preencher o espaço. Veja um exemplo:

```
Memória:            Espaço:                Resultado:
1111222233334444   +----------------+   +----------------+
5555666677778888   |                |   |1111222233334444|
99990000aaaabbbb   |                |   |5555666677778888|
ccccddddeeeeffff   |                |   |99990000aaaabbbb|
                   |                |   |ccccddddeeeeffff|
                   |                |   |1111222233334444|
                   |                |   |5555666677778888|
                   |                |   |99990000aaaabbbb|
                   |                |   |ccccddddeeeeffff|
                   +----------------+   +----------------+
``` 

É chamado de espelhamentoporque o que se vê é uma "imagem" duplicada da memória. Se você modifica uma das "imagens" 
(se é na RAM), as duas são alteradas. Mas repare que o "reflexo" não é invertido!

A RAM dos sistemas do Master System e do Game Gear são invertidas. Os 8KB ocupam um espaç ode 16KB. Para não causar 
confusão, evitamos utilizar o "reflexo"; Na maioria das vezes nós agimos como se ele não existisse.

### Mapa de memória do Master System

O processador Z80 tem um espaço de endereçamento de 16 bits. Isto literalmente significaque há 16 pinos nele que 
seleciona um endereço de memória; Isto significa que ele pode selecionar endereços entre $0000 e $ffff (64KB). Isto é 
dividido (geralmente) nos seguintes espaços:

|   Endereço  |    Conteúdo  | Tamanho |
|-------------|--------------|---------|
| $0000-$00ff |     ROM      |   1KB   |
| $0100-$3fff | ROM (slot 1) |  15KB   |
| $4000-$7fff | ROM (slot 2) |  16KB   |
| $8000-$bfff | ROM (slot 3) |  16KB   |
| $c000-$dfff |     RAM      |   8KB   |
| $e000-$ffff | ROM (espelho)|   8KB   |

Para jogos maiores, os espaços de ROM de 1 a 3 podemo ser selecionados ("mapeados") pelo jogo. Para jogos menores, 
como os de 32KB, os primeiros 1KB mais os slots 1 e 2 são o suficiente para conter todos os 32KB, e o slot 3 fica vazio.

## Registradores

Registradores são um tipo especial de memória dentro da CPU. Eles podem ser acessados muito mais rapidamente e as 
operações podem ser realizadas com eles como operandos. Quase sempre quando você quiser fazer algo na RAM ou na ROM, 
você precisa transferir o conteúdo para um ou mais registradores, realizar a operação, e então (talvez) transferir o 
resultado de volta.

Existem aproximadamente 22 registradores no Z80, todos com um nome curto de uma ou duas letras. Aqueles com uma única 
letra no nome podem conter 8 bits (um byte); os que contêm duas letras no nome podem armazenar 16 bits (dois bytes). 
Segue um guia rápido:

| Registrador | Nome | Uso |
|-------------|------|-----|
| `a` | Acumulador | Este é o principal registrador de aritmética de 8 bits. Muitas instruções podem operar apenas em `a` |
| `f` | Flag (sinalizador) | Os vários bits contém flags que sinalizam o que aconteceu durante as operaçẽos recentes, permitindo ramificação condicional. |
| `b`, `c`, `d`, `e`, `h`, `l` | Registradores de propósito geral | Nós podemos fazer o que quiser com estes, porque (normalmente) eles não importam. Eles podem ser acessados individualmente como registradores de 8 bits, ou em pares como `bc`, `de` e `hl`. O `hl` é um "super registrador" mais ou menos como `a`, porque ele pode fazer fazer mais operações aritméticas. |
| `ix`, `iy` | Index registers (Registradores-Índice) | Estes dois registradores de 16 bits são úteis como índice de alguma outra coisa, porque você pode trabalhar com o que eles apontam sem quase mudar seus valores. E eles também são utilizáveis como registradores de 16 bits. |
| `pc` | Program Counter (Contador do programa) | Este é o registrador que o Z80 acompanha onde está no programa . |
| `sp` | Stack Pointer (Ponteiro de pilha) | Acompanha a **pilha** (que falaremos em breve) |
| `i` | Interrupt Page Address | Não precisamos nos preocupar com este, o Master Sytem não o usa |
| `r` | Refresh (atualizar) | Este registrador é para manter a memória atualizada, e normalmente não é útil. |
| `a'`, `f'`, `b'`, `c'`, `d'`, `e'`, `h'` e `l'` | Registradores alternativos | É possível trocá-los com suas versões regulares, mas não individualmente. |

Alguns dos registradores de 8 bits podem ser pareados para produzir um de 16 bits: `b` e `c` se tornam `bc`, `d` e `e` 
se tornam `de`, e `h` e `l`  formam `hl`. Você também pode particionar `ix` em `ixh` e `ixl`, e similar para `iy`, mas 
raramente será feito.  

## Portas

O Z80 pode fazer entrada/saída de 1 byte de/para qualquer uma entre 256 portas, o que é útil para mandar e receber dados
 de outras partes do sistema. No Master System, estas são as portas relevantes - repare que todas elas estão espelhadas 
em outras áreas, mas se torna confuso se voCẽ usar os espelhos, então não nos aprofundaremos isto.

 |   Porta    |    Entrada  | Saída |
 |-------------|--------------|---------|
 | `$3e` | - | Controle de memória |
 | `$3f` | Porta de controle de E/S | Porta de controle de E/S |
 | `$7e` | V counter | - |
 | `$7f` | H counter | **PSG** |
 | `$be` | - | **VDP (dados)** |
 | `$bf` | - | **VDP (controle)** |
 | `$dc`, `$dd` | **Controles** | Controles |

Os únicos que serão utilizados são os destacados em negrito. Para por um programa rodando, você realmente só precisa dos
 VDPs, na verdade.
 
# Passo a passo pelo Hello World

Esta seção via passar por cada linha de código no código de exemplo do "Hello World" que compilamos na configuração.

**Diretivas do assembler**  informam ao assembler como proceder.

**Boot** mostra as primeiras instruções.

**Tratamento do botão de pausa** é um endereço fixo que necessita de tratamento especial.

**Inicialização** é onde reiniciamos vários aspectos do sistema para um estado "vazio".

**Iniciando os gráficos** mostra o carregamento de dados de tiles, a paleta e o mapa de tiles.

**Encerramento** é o passo final para manter o controle.

## Diretivas do assembler

Você deve ter o seu `Hello World.asm` aberto no seu editor de texto. Vamos ver o que ele faz.

## Configuração de banco de memória do WLA DX

Na seção [Memórias](#memory) nós aprendemos sobre o mapeamento de memória no Z80. Se nós queremos usar mais de 48KB de 
ROM, nós precisamos informar ao WLA DX tudo sobre os espaços; no entanto, para um caso mais simples como este, tudo que 
precisamos é informar onde fica e quão grande é a ROM.  

```asm
;==============================================================
; Configuração do banco de memória do WLA-DX 
;==============================================================
.memorymap
defaultslot 0
slotsize $8000
slot 0 $0000
.endme

.rombankmap
bankstotal 1
banksize $8000
banks 1
.endro
```

Este grande [Comentário](#comment) aqui nos relembra para que serve e para "dividir" o arquivo em linhas horizontais. 
É apenas uma questão de gosto.

basicamente, as diretivas dizem que queremos um "banco" de memória ROM de 32KB ($8000 bytes) e que ele começa no 
endereço `$0000` no mapa de memória (espaço 0).

## Etiqueta SDSC 

```asm
;==============================================================
; Etiqueta SDSC e cabeçalho de ROM Sega Master System.
;==============================================================
.sdsctag 1.10,"Hello World!","SMS programming tutorial program (bugfixed)","Maxim"
```

Este é um dos detalghes que você deve incluir no seu software caseiro. Gentilmente adicionado ao WLA DX depois de ser 
inventado no [fórum do SMSPower!](http://www.smspower.org/forums/), permite a você embutir informações úteis sobre o 
seu programa - o título, versão, data, autor, e notas - na imagem ROM resultante. É outra diretiva do WLA DX - você 
insere a versão, nome, notas e o nome do autor e ele preenche os dados pra você.

Adicionando esta etiqueta você também incita o WLA a inserir um **cabeçalho válido de ROM de Master System**. Isto é 
algo encontrado em (quase) todas as roms de Master System e que verifica que o cartucho está correto quando jogado num 
sistema original. Não é absolutamente necessário, mas você quer que seu programa funcione num Master System de verdade, 
não é?

Você pode adicionar esta diretiva em qualquer lugar do seu código. Pondo no início relembra de onde está, e que você 
precisa atualizá-lo.

## Onde pôr o código

```asm
.bank 0 slot 0
.org $0000
```

Isto diz para o WLA DX que o código (e os dados) que estamos especificando devem ir no banco 0 da ROM (mesmo que só 
exista um banco de ROM) e qye este banco estará no espaço 0 (mesmo que só tenhamos um espaço). Então nós dizemos que nós
 queremos que o código comece bem no começo (localizado em `$0000`).

## Boot

Até o momento só apresentamos as diretivas do WLA DX. Agora vem algum código assembly:

```asm
;==============================================================
; Setor de Boot
;==============================================================
    di              ; desabilitar interrupções
    im 1            ; Modo de interrupção 1
    jp main         ; pula para o programa principal
```

Quase todos os programas do Master System começam desta forma.

As primeiras três linhas são apenas um comentário para tornar claro o que está acontecendo aqui.

A primeira instrução real do Z80 é a `di` (**Disable Interrupts**). Interrupções são algo que fazem com que a execução 
do código seja direcionada para outro lugar ("interrompendo" o fluxo do programa) para tratar algo, e nós não queremos 
que isto aconteça até que estejamos prontos.

A próxima é `im 1`. Esta inicializa o **Interrupt Mode** do Z80. Por diversos motivos, o único modo que faz sentido para
 o programador do Master System é o modo 1. Então é desta forma que faremos.  

Agora que nós fizemos as duas coisas mais importantes (impedir que o fluxo do nosso programa seja afetado) nós podemos 
seguir com o código. Por motivos técnicos, o início da ROM é importante e reservado para certas coisas, então o nosso 
código "comum" deve estar em outro lugar. Então nós queremos "pular" para onde quer que ele esteja. Em outro momento, 
defini o [Rótulo](#label) chamado "main". A COU então segue executando as instruções depois deste rótulo. Chegaremos lá 
num instante.

Repare que foi adicionado um [Comentário](#comment) em cada linha informando o que cada instrução faz. estes parecerão 
muito verbosos. Uma vez que você esteja num nível mais avançado, você percebe que são um tanto desnecessários, já que 
eles não dizem nada que as instruções não digam elas mesmas.
  
## Tratamento do botão de Pausa

```asm
.org $0066
;==============================================================
; Tratamento do botão de pausa
;==============================================================
    ; Não faça nada
    retn
```

Lembra como as interrupções foram desabilitadas agorinha? Bem, tem algumas interrupções que você *não pode desabilitar*,
 as oficialmente chamadas **NMIs** (Non-Maskable Interrupts). No Master System elas não são ruins, são apenas usadas 
 para o botão de pause.

Sempre que o botão de pause for pressionado, a execução do código para o que quer que esteja fazendo e pula para 
endereço `$0066`. Então, isto executa qualquer código que esteja ali até que encontre uma instrução `retn` ( **Ret**orne
 da **N**MI), quando retornará para o que quer que estivesse executando quando foi pressionado. Se nós não queremos 
 fazer nada de especial para tratar o botão de pause, nós devemos  então pôr este comando bem no `$0066`. A diretiva 
 `.org $0066` diz ao WLA DX que este código deve estar em `$0066`.  

## Inicialização

Agora que terminamos de lidar com os detalhes especiais que precisam estar no início, nós podemos votlar ao nosso 
programa "principal" (main). A primeira coisa que devemos fazer é inicializar várias partes do sistema.

### Início do programa main - ponteiro de pilha

```asm
;==============================================================
; Programa main
;==============================================================
main:
    ld sp, $dff0
```

Esta é o nosso rótulo "main".

Talvez você se lembre de que o registrador `sp` é um registrador especial chamado **Stack Pointer** (ponteiro de pilha), 
e que a o conceito de pilha ainda não foi explicado. E ainda não será, pois ainda não o usaremos ainda; mas basta dizer 
que a pilha pega uma parte da RAM disponível, e nós precisamos informar que RAM usar. Aqui, dizemos para usar uma área 
de memória que *termina*  no endereço `$dff0`. A quantidade que a pilhavai usar varia, mas informando onde termina a 
RAM, nós podemos usar a memória do início da RAM tendo a certeza de que não vamos sobrepor a pilha.

Como um lembrete, o Master System tem 8KB de RAM, localizados entre os endereços hexadecimais `$c000` `dfff`, e 
espelhados entre `$e000` e `$ffff`. Nós não configuramos o fim da pilha para o último endereço disponível na RAM (`$dfff`) 
por um motivo importante que será abordado mais tarde.

Nós carregamos (**l**oa**d**) o registrador com um valor usando a instrução ``ld <registrador>, <valor>``. Você pode 
ler esta instrução como "carregue o ponteiro de pilha com $dff0".

### Configurando os registradores VDP - transfernêcia de bloco

Agora vamos a um detalhe técnico. O VDP (Video Display Processor / Processador de Vídeo) é o chip gráfico do Master 
System. Dentro dele, há um conjunto de registradores e alguma RAM que controlamos através de duas portas, `$be` e `$bf`. 
A documentação "Sega Master System VDP Documentation", por Charles McDonald é uma boa (e avançada) documentação sobre 
como isto funciona, mas é muita coisa para se aprofundar agora. Basta dizer que abaixo no programa nós inserimos um 
bloco de dados que podemos utilizar para configurar estes registradores para valores iniciais adequados:

```asm
; Dados de inicialização do VDP
VdpData:
.db $04,$80,$00,$81,$ff,$82,$ff,$85,$ff,$86,$ff,$87,$00,$88,$00,$89,$ff,$8a
VdpDataEnd:
```

`.db` é uma diretiva do WLA DX que instrui o compilador para inserir na ROM os bytes que você escreveu sem nenhuma 
modificação. A diretiva `.db` deve ser sucedida por uma lista de valores separados por vírgula (CSV) que serão 
interpretados como bytes e então armazenados.

Para escrever nos registradores VDP, nós devemos dar a saída desses dados para a porta de controle do VDP, que é a porta
 `$bf`. O que faremos agora é usar uma das instruçeõs do Z80 para **transferência de blocos**. Estas instruções pegam 
os valores armazenados em certos registradores e transferem (copiam ou dão saída) um bloco de dados de acordo com estes 
valores. Aqui está o código:  

```asm
    ;==============================================================
    ; Configura os registradores do VDP
    ;==============================================================
    ld hl,VdpData
    ld b,VdpDataEnd-VdpData
    ld c,$bf
    otir
```

`otir` significa "jogue na saída os `b` bytes de dados  começando no endereço de memória `hl` para a porta especificada 
em `c`". Isto é ótimo - nós podemos descobrir todos estes, como você pode ver. Um truque é que nós subtraímos dois 
rótulos - "VdpDataEnd-VdpData". Já que os rótulos são traduzidos para endereços, a diferença entre estes dois rótulos 
será o número de bytes entre eles.

Há outras instruções de transferência de blocos, mais notavelmente a `ldir`, que pode ser usada para copiar de uma 
localização da memória para outra.

## Limpando a VRAM - Acesso de escrita na VRAM, laços, e saltos condicionais

Não é possível prever o que está carregado na RAM do VPD de antemoã, e se não limparmos, nossa tela carregará dados 
estranhos. (Na verdade, em um Master System real, vai ter o logo da SEGA carregado na BIOS). Então, vamos carregar cada 
byte como zero.

```asm
    ;==============================================================
    ; Limpar VRAM
    ;==============================================================
    ; 1. Configura o acesso de escrita da VRAM para 0 escrevendo $4000 ORed com $0000
    ld a,$00
    out ($bf),a
    ld a,$40
    out ($bf),a
    ; 2. Escreve 16KB de zeros
    ld bc, $4000    ; Contador para 16KB de VRAM
    ClearVRAMLoop:
        ld a,$00    ; Valor para escrever
        out ($be),a ; Saída para a VRAM
        dec bc
        ld a,b
        or c
        jp nz,ClearVRAMLoop
```
Eita, veja só! É um belo trecho de código, meio assustador, na verdade. Mas não é tão ruim, na verdade - você vai rir 
de algo assim logo logo. Vamos ver o que está acontecendo aqui.

Primeiro, nós temos que nos comunicar com o VDP e informar a ele que queremos escrever na VRAM. (VRAM é como chamamos 
a **RAM** do **V**DP). Para fazê-lo, temos de informar em qual o endereço que desejamos escrever, e solicitar a escrita.
 Como temos 16KB de VRAM, nós vamos precisar de 14 bits (2^14 = 16384 = 16KB) para o endereço.Os últimos dois bits que 
completam um número de 16KB (2 bytes) são usados para sinalizar nossas intenções. Para pegar o número final, nós podemos
 usar um cálculo OU - o número $4000 só contém os bits necessários para informar ao VDP que queremos escrever na VRAM, e
 se nós fizermos um OU com o endereço nós teremos o número final para enviar ao VDP. No nosso caso, nós queremos começar 
 no endereço $0000, então o número final é $4000. (Tente numa calculadora hexadecimal!)
 
 Nós temos que fazer esta saída (**out**put) para a porta `$bf`, a porta controladora da VDP como dito há algum tempo. 
No entanto, nós temos que considerar a *ordenação dos bytes*.

Muitos sistemas de computador modernos tem unidades de armazenamento baseadas em byte. Contudo, elas também querem lidar
 com números maiores - normalmente 32-bits ou 64-bits nas arquiteturas modernas - então quando estes números estão sendo
 transferidos como múltiplos bytes, é preciso ter alguma convenção quanto a maneira que eles serão transmitidos. No Z80,
 e no VDP do Master System, a ordem de transferência é o *bytes menos significativo* é informado primeiro, então se 
você quer um número como $4000, por exemplo, você comunica na ordem $00, $40. Isto também é conhecido como 
"**little-endian**ess" (N.T.: A tradução literal de **endian** é extremidade). Então é por isso que na parte 1 acima nós
escrevemos $00, e depoi $40.

Porque nós não podemos escrever `out ($bf), $4000`? Porque o Z80 não sabe como. Há algumas restrições em como você pode 
manipular os dados. Neste caso, nós só podemos escrever um byte por vez, e o dado tem que sair do registrador `a`. (Há 
outras opções possíveis, mas esta é a mais fácil.)

Agora nós configuramos o VDP para receber dados. Nós enviamos estes dados escrevendo para a porta `$be`, a porta de 
dados do VDP. Quano esta porta recebe estes dados, ela escreve na VRAM e então (de maneira bem prática) move para o 
próximo byte da VRAM, então nós podemos apenas enviar um fluxo de bytes de dados e será escrito consecutivamente na VRAM.
 Em seguida, nós precisamos enviar 16384 zeros. A maneira de fazer isto é começar no endereço $4000 (=16384), então 
 escrever um zero e decrementar nosso contador. Então repetimos isto até o nosso contador estar zerado. E isto é o que 
 fazemos na parte 2.

Primeiro, nós armazenamos $4000 no **par de registradores** `bc`. Em seguida nós encontramos com outra instrução do Z80 
que desejamos ter. Nós queremos fazer um laço de repetição, decrementando `bc` em 1 a cada vez e verificando se seu 
valor é zero - mas enquanto há uma instrução para **dec**rementar um par de registradores em um, não há uma função 
embutida para checar se seu valor é zero. Portanto, temos que verificar nós mesmos utilizando a eficiente instrução `or`.
 Isto combina o byte atual em `a` com outro byte, nos retornando um resultado binário 1 se qualquer uma das duas 
 entradas for 1. Por isso ele só resulta zero se as duas entradas forem zero.
 
A instrução `or` (ou) também afeta o sinalizador `z`. Este é o um bit no registrador `f` (**f**lag / sinalizador), que 
será 1 se o último cálculo for igual a zero - mas apenas para alguns cálculos. (Veja o Manual do Usuário do Z80 para ver
 quais sinalizadores são afetados por cada instrução). Nós podemos usar este sinalizador em um salto condicional para 
 continuar escrevendo até o contador chegar a zero.
    
Um salto condicional é similar a um salto comim, com exceção de que ele só salta se a condição especificada for 
atendida. Todos estes são baseados no registrador `f`; o conteúdo do registrador `f` é afetado por diferentes instruções
 de diferentes maneiras, e algumas instruções não o afetam. Portanto, o sinalizador pode ser visto a alguma distância de
  onde foi atribuído.
  
Aqui está a lista de condições disponíveis:
  
| Condição | Significa | em Português | Descrição |
|----------|-----------|--------------|-----------|
|   `nz`   | Not Zero  |  Não zerado  | O resultado do último cálculo foi diferente de zero | 
|   `z`    |   Zero    |  Zerado  | O resultado do último cálculo foi igual a zero | 
|   `nc`   | No carry (Overflow)  |  Sem estouro | O resultado do último cálculo não foi além dos limites entre $ff e $00, ou vice versa | 
|   `c`    | Carry (Overflow) |  Estouro  | O resultado do último cálculo foi além dos limites entre $ff e $00, ou vice versa | 
|   `po`   | Parity Odd  |  Paridade ímpar  | O resultado do último cálculo tem um número ímpar de bits | 
|   `pe`   | Parity Even  |  Paridade par  | O resultado do último cálculo tem um número par de bits | 
|   `p`    | Positive  |  Positivo  | O resultado do último cálculo ficou entre $00 e $7f | 
|   `m`    | Minus (negative) |  Negativo  | O resultado do último ficou entre $80 e $ff (com sinal) |

As próximas condições serão explicadas no futuro, já que são um tanto confusas.

Portanto, nós podemos escrever algo `bc` vezes usando um laço de repetição como o mostrado acima. Aqui está uma versão 
com comentários descrevendo o que acontece de maneira mais verbosa:

```asm
    ; 2. Escreve 16KB de zeros
    ld bc, $4000    ; Contador para 16KB de VRAM
    ClearVRAMLoop:
        ld a,$00    ; Valor a escrever
        out ($be),a ; Escrita para a VRAM
        dec bc      ; Decrementa o contador
        ld a,b      ; Pega o bit mais alto
        or c        ; Combina com o mais baixo
        jp nz,ClearVRAMLoop ; Repita enquanto o  o resultado for diferente de zero.
```

Assim, esta seção marcou todos os bits da VRAM para zero. Agora nós temos um espaço em branco para começar a inserir 
nossos dados.
 
## Iniciando os gráficos

Os gráficos do Master Systtem são formados por quato tipos diferentes:

- Paleta
- Tiles
- Mapa de tiless
- Tabela de sprites

Para fazer o plano de fundo, são usadosuma combinação de paleta, tiles e mapa de tiles. Para fazer os *sprites*, - i.e.
 o jogador, inimigos, projéteis, etc - utilizamos a paleta, os tiles e a tabela de sprites.
 
 Para o Hello World, nós estamos usando apenas o plano de fundo. Todos os elementos gráficos são armazenados em uma RAM 
especial pertencente ao VDP (o chip gráfico). Para levá-los até lá, a CPU comunica com o VDP usando as [portas](#port) 
`$bf` e `$be`.

### Paleta

A paleta define que cores nós podemos usar. Para o Hello World, temos apenas duas cores: preto e branco. Similar à 
inicialização dos dados do VDP, nós queremos armazenar na ROM os dados necessários, e transferí-los para o VDP.

```asm
PaletteData:
.db $00,$3f ; Preto, branco
PaletteDataEnd:
``` 

Nós devemos dizer para o VDP que queremos escrever para a paleta (algumas vezes chamada de CRAM, **C**olour **RAM**). 
Nós fazemos isto de maneira similar a como nós configuramos os endereços da VRAM antes de escrevermos os 16KB de zeros, 
mas desta vez o endereço é o índice da paleta (nós queremos começar em 0), e os **dois** bits mais altos, que alcançamos
 o ORing com $c000 (e é $4000 para escolher o endereço de escrita da VRAM).
 
 ```asm
     ;==============================================================
     ; Carregar paleta
     ;==============================================================
     ; 1. Configura o endereço de escrita da VRAM para a CRAM (paleta) endereço 0 (para o índice 0 da paleta)
     ; escrevendo $c000 ORed com $0000
     ld a,$00
     out ($bf),a
     ld a,$c0
     out ($bf),a
``` 

Porque nós temos uma pequena quantidade de dados (menos de 256 bytes - na verdade, é só dois bytes) nós podemos usar a 
instrução `otir` novamente, para escrever para a [porta](#port) de dados `$be`:

```asm
    ; 2. Escreve os dados de cores
    ld hl,PaletteData
    ld b,(PaletteDataEnd-PaletteData)
    ld c,$be
    otir
```

Nós não estamos escrevendo aos outras 14 cores na paleta de plano de fundo, ou as 16 cores da paleta de sprite, porque 
não estamos usanto estas.

#### A Paleta

A paleta define que cores podemos utilizar. Há na verdade duas paletas - uma para o plano de fundo, e outra para os 
sprites. (A paleta dos sprites também pode ser utilizada no plano de fundo). Cada paleta contém 16 entradas. Esta é uma
 janela de paleta no Meka:
 

 ![Paleta](../assets/lesson1/palette.png)
 
 *Paleta - http://www.smspower.org/maxim/uploads/HowToProgram/palette.png*
 
As primeiras dezesseis cores são da paleta de plano de fundo, e a segunda sequência de 16 cores são da paleta de sprite.

Cada pixel de cada paleta é representado por 4 bits, que nos dá um número de 0 a 15. Este número é usado para selecionar
 que cor utilizar. É muito parecido com "Paint by Number":
 
 ![Aplicação da paleta](../assets/lesson1/palette-apply.png)
 
 Cada entrada na paleta é uma das 64 cores possíveis no Master System:
 
 ![Cores](../assets/lesson1/64colours.png)
 
Para selecionar uma cor, voCê deve escolher um número entre 0 e 3 para cada um entre os canais de cor vermelho, verde, e
 azul. Estes se combinam em um byte:
 
 | Bit: | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
 |------|---|---|---|---|---|---|---|---|
 |   %  | - | - | B | B | G | G | R | R |
 
Sendo: **R** = red (vermelho), **G** = green (verde), **B** = blue (azul). Os bits 6 e 7 **não** são utilizados.
 
Então, para o nosso exemplo, se há um pouco de azul, nenhum verde, e muito vermelho, a cor deve ser `%00010011`. Aqui 
está a lista de tpdas as cores:

| Hex | Dec | Binário  |  R   | G   | B   | HTML    |     Nome em Inglês      |
|-----|----|-----------|------|-----|-----|---------|-------------------------|
| $00 |  0 | %00000000 |    0 |   0 |   0 | #000000 | Black                   |
| $01 |  1 | %00000001 |   85 |   0 |   0 | #550000 | Bulgarian Rose          |
| $02 |  2 | %00000010 |  170 |   0 |   0 | #aa0000 | Dark Candy Apple Red    |
| $03 |  3 | %00000011 |  255 |   0 |   0 | #ff0000 | Red                     |
| $04 |  4 | %00000100 |    0 |  85 |   0 | #005500 | Dark Green              |
| $05 |  5 | %00000101 |   85 |  85 |   0 | #555500 | Army Green              |
| $06 |  6 | %00000110 |  170 |  85 |   0 | #aa5500 | Windsor Tan             |
| $07 |  7 | %00000111 |  255 |  85 |   0 | #ff5500 | Orange                  |
| $08 |  8 | %00001000 |    0 | 170 |   0 | #00aa00 | Islamic Green           |
| $09 |  9 | %00001001 |   85 | 170 |   0 | #55aa00 | Kelly Green             |
| $0a | 10 | %00001010 |  170 | 170 |   0 | #aaaa00 | Limerick                |
| $0b | 11 | %00001011 |  255 | 170 |   0 | #ffaa00 | Chrome Yellow           |
| $0c | 12 | %00001100 |    0 | 255 |   0 | #00ff00 | Green                   |
| $0d | 13 | %00001101 |   85 | 255 |   0 | #55ff00 | Bright Green            |
| $0e | 14 | %00001110 |  170 | 255 |   0 | #aaff00 | Spring Bud              |
| $0f | 15 | %00001111 |  255 | 255 |   0 | #ffff00 | Yellow                  |
| $10 | 16 | %00010000 |    0 |   0 |  85 | #000055 | Oxford Blue             |
| $11 | 17 | %00010001 |   85 |   0 |  85 | #550055 | Imperial Purple         |
| $12 | 18 | %00010010 |  170 |   0 |  85 | #aa0055 | Jazzberry Jam           |
| $13 | 19 | %00010011 |  255 |   0 |  85 | #ff0055 | Folly                   |
| $14 | 20 | %00010100 |    0 |  85 |  85 | #005555 | Midnight Green          |
| $15 | 21 | %00010101 |   85 |  85 |  85 | #555555 | Dark Gray               |
| $16 | 22 | %00010110 |  170 |  85 |  85 | #aa5555 | Rose Vale               |
| $17 | 23 | %00010111 |  255 |  85 |  85 | #ff5555 | Sunset Orange           |
| $18 | 24 | %00011000 |    0 | 170 |  85 | #00aa55 | Jaeger Green            |
| $19 | 25 | %00011001 |   85 | 170 |  85 | #55aa55 | May Green               |
| $1a | 26 | %00011010 |  170 | 170 |  85 | #aaaa55 | Brass                   |
| $1b | 27 | %00011011 |  255 | 170 |  85 | #ffaa55 | Rajah                   |
| $1c | 28 | %00011100 |    0 | 255 |  85 | #00ff55 | Malachite               |
| $1d | 29 | %00011101 |   85 | 255 |  85 | #55ff55 | Screamin' Green         |
| $1e | 30 | %00011110 |  170 | 255 |  85 | #aaff55 | Inchworm                |
| $1f | 31 | %00011111 |  255 | 255 |  85 | #ffff55 | Icterine                |
| $20 | 32 | %00100000 |    0 |   0 | 170 | #0000aa | Duke Blue               |
| $21 | 33 | %00100001 |   85 |   0 | 170 | #5500aa | Indigo                  |
| $22 | 34 | %00100010 |  170 |   0 | 170 | #aa00aa | Purple                  |
| $23 | 35 | %00100011 |  255 |   0 | 170 | #ff00aa | Fashion Magenta         |
| $24 | 36 | %00100100 |    0 |  85 | 170 | #0055aa | Cobalt Blue             |
| $25 | 37 | %00100101 |   85 |  85 | 170 | #5555aa | Liberty                 |
| $26 | 38 | %00100110 |  170 |  85 | 170 | #aa55aa | Purpureus               |
| $27 | 39 | %00100111 |  255 |  85 | 170 | #ff55aa | Brilliant Rose          |
| $28 | 40 | %00101000 |    0 | 170 | 170 | #00aaaa | Tiffany Blue            |
| $29 | 41 | %00101001 |   85 | 170 | 170 | #55aaaa | Cadet Blue              |
| $2a | 42 | %00101010 |  170 | 170 | 170 | #aaaaaa | Light Gray              |
| $2b | 43 | %00101011 |  255 | 170 | 170 | #ffaaaa | Melon                   |
| $2c | 44 | %00101100 |    0 | 255 | 170 | #00ffaa | Medium Spring Green     |
| $2d | 45 | %00101101 |   85 | 255 | 170 | #55ffaa | Medium Aquamarine       |
| $2e | 46 | %00101110 |  170 | 255 | 170 | #aaffaa | Mint Green              |
| $2f | 47 | %00101111 |  255 | 255 | 170 | #ffffaa | Pastel Yellow           |
| $30 | 48 | %00110000 |    0 |   0 | 255 | #0000ff | Blue                    |
| $31 | 49 | %00110001 |   85 |   0 | 255 | #5500ff | Electric Ultramarine    |
| $32 | 50 | %00110010 |  170 |   0 | 255 | #aa00ff | Vivid Violet            |
| $33 | 51 | %00110011 |  255 |   0 | 255 | #ff00ff | Magenta                 |
| $34 | 52 | %00110100 |    0 |  85 | 255 | #0055ff | Blue Moon               |
| $35 | 53 | %00110101 |   85 |  85 | 255 | #5555ff | Very Light Blue         |
| $36 | 54 | %00110110 |  170 |  85 | 255 | #aa55ff | Lavender Indigo         |
| $37 | 55 | %00110111 |  255 |  85 | 255 | #ff55ff | Shocking Pink           |
| $38 | 56 | %00111000 |    0 | 170 | 255 | #00aaff | Vivid Cerulean          |
| $39 | 57 | %00111001 |   85 | 170 | 255 | #55aaff | Picton Blue             |
| $3a | 58 | %00111010 |  170 | 170 | 255 | #aaaaff | Baby Blue Eyes          |
| $3b | 59 | %00111011 |  255 | 170 | 255 | #ffaaff | Rich Brilliant Lavender |
| $3c | 60 | %00111100 |    0 | 255 | 255 | #00ffff | Cyan                    |
| $3d | 61 | %00111101 |   85 | 255 | 255 | #55ffff | Electric Blue           |
| $3e | 62 | %00111110 |  170 | 255 | 255 | #aaffff | Celeste                 |
| $3f | 63 | %00111111 |  255 | 255 | 255 | #ffffff | White                   |

Então há 64 cores possíveis no Master System, mas temos que selecionar 16 delas para cada paleta, apenas - você não pode
 usar mais do que isso sem truques especiais.

#### Escrevendo para a paleta

Para escrever na paleta, inicialize o endereço usando a porta de controle do VDP, com os dois bits mais altos 
configurados (i.e. endereço ORed com $c000). Se você configurar um endereço maior do que 32, o endereço é quebrado (wrap).

#### Transparência

O índice 0 da paleta é especial Para os sprites, é sempre transparente - a cor que você escolhe para nunca ser usada 
(para sprites, ela ainda pode ser usada para outras coisas). Para os tiles de plano de fundo, ele é desenhado, mas 
quando o tile está marcado para ser desenhado na frente de sprites (veja [Mapa de tiles](#tilemap)), ele é desenhado 
atrás dos sprites enquanto as outras cores do plano de fundo são desenhadas na frente, então é tipo uma "transparência". 

#### Diferenças com o Game Gear

A paleta do Game Gear é exatamente igual a de Master System, exceto:

- Cada canal de cor tem 4 bits, não 2, dando 2^12 = 4096 cores possíveis.
- Cada entrada na paleta então toma dois bytes.
    - Então há 64  bytes de CRAM em vez de 32
    - Cada entrada toma dois bytes consecutivos, no formato:
    
| Byte: | 1 | 1 | 1 | 1 | 1 | 1 | 1 | 1 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
|-------|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|

| Bit: | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
|-------|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
|   %   | - | - | - | - | B | B | B | B | G | G | G | G | R | R | R | R |

### Tiles

Tiles são os blocos de construção para os gráficos de Master System. Nós devemos definir todos os gráficos na forma de 
tiles 8x8; porque estamos desenhando texto, nós vamos definir um tile por caractere, para fornecer a nossa fonte. Os 
dados da fonte são armazenados na ROM; é um pouco grande, então não será reproduzido por completo:

```asm
FontData:
.db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
.db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
...
.db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
FontDataEnd:
```

Os tiles são carregados na VRAM no endereço $0000. Nós configuramos o endereço como antes:

```asm
    ;==============================================================
    ; Carrega os tiles (de fonte)
    ;==============================================================
    ; 1. Configura o endereço de escrita da VRAM para o tile de índice 0
    ; escrevendo $4000 ORed com $0000
    ld a,$00
    out ($bf),a
    ld a,$40
    out ($bf),a
```

Como os dados de tiles são um tanto grandes, nós não podemos simplesmente usar a instrução `otir` para escrevê-los, 
porque `otir` só pode contar com o registrador `b`, que tem 8 bits de largura, então o máximo é 256. No lugar, devemos 
utilizar um par de registradores, de maneira similar a como usamos um para contar pelos 16KB quando limpamos a VRAM. A 
diferença é que desta vez estamos lendo os dados da ROM em vez de sempre escrever zeros.

```asm
    ; 2. Escreve os dados de tile
    ld hl,FontData              ; Local dos dados de tile
    ld bc,FontDataEnd-FontData  ; Contador para o número de dados a escrever
    WriteTilesLoop:
        ld a,(hl)        ; Pega os bytes de dados
        out ($be),a      ; Escreve
        inc hl           ; Adiciona um em  hl para que ele aponte para o próximo byte de dados
        dec bc           ; Decrementa o contador e repete até que seja zero
        ld a,b
        or c
        jp nz,WriteTilesLoop
```

Este é o trecho mais complexo até agora, então vamos com calma. As primeiras duas linhas estão configurando dois pares 
de registradores - `bc` e `hl` - com alguns números. `hl` armazena a localização dos dados, e `bc` o tamanho destes 
dados (em bytes).

Então nós lemos um byte de dados no registrador `a`. Os parênteses em volta do `(hl)` significam *indireção* - isto 
significa que nós não carregamos `a` com o *valor* de `hl` (isto não faria sentido, já qie são números diferentes de 
bits, de qualuer forma), no lugar disso nós carregamos `a` com o valor no *endereço de memória contido em* `hl`. Isso 
significa que nós vamos pegar os dados do tile que queremos. Em seguida nós escrevemos isto para a [porta](#port) de 
dados do VDP, então nós copiamos efetivamente isto da ROM para um registrador, e então para a VRAM.

Em seguida, nós **inc**rementamos o `hl`. Isto significa que nós adicionamos um a ele. Desta maneira, da próxima vez que
 lermos o registrador, ele nos dará o próximo byte. Não há necessidade de incrementar o endereço da VRAM porque o VDP 
 faz esta operação automaticamente.
 
Finalmente nós decrementamos o o contador e fazemsos um laço de repetição se o resultado não for zero, como antes. Isto 
significa que nós vamos escrever exatamente o número exato de bytes de dados de tile.

Isto é similar a como o `otir` trabalha internamente; a diferença é, a nossa versão usa um contador de 16 bits, e nós 
especificamos um número de porta toda vez no lugar de usar o registrador `c` para informar isto. 

#### Formato de dados

Todos os gráficos no Master Systema são construídos através de tiles de 8x8 pixels.

![Montagem dos Tiles](../assets/lesson1/tilemakeup.png)

*Montagem de tiles - http://www.smspower.org/maxim/uploads/HowToProgram/tilemakeup.png*

Cada pixel é um índice da paleta com valores entre 0 e 15, i.e. 4 bits.

![Bits do pixel](../assets/lesson1/pixel-bits.png)

*Bits de pixel - http://www.smspower.org/maxim/uploads/HowToProgram/pixel-bits.png*

Sendo assim, um tile inteiro é uma pilha de bits:

![Bits do tile](../assets/lesson1/tile-bits.png)

*Bits do tile - http://www.smspower.org/maxim/uploads/HowToProgram/tile-bits.png*

Os dados do tile estão num formato *plano*, particionados por linha do tile. Isto significa que o primeiro byte contém o
 *bit menos significante*, bit 0 de cada pixel, o segundo byte contém o bit 1, o terceiro contém o bit 2, e o quarto o 
 bit 3. Assim, os primeiros íxels são representados pelos primeiros 4 bytes de dados, particionados pelo "plano de bit".
  O processo é repetido para as linhas consecutivas do tile, produzindo 32 bytes no total.
  
![Tile planar](../assets/lesson1/tile-planar.png)

*Tile planar - http://www.smspower.org/maxim/uploads/HowToProgram/tile-planar.png*

#### Conversão de dados

Para converter imagens para este formato, é sensato pré-processá-los antes de incluí-las na ROM. Você pode fazer sua 
própria ferramenta para isso, ou (se você usar Windows) você pode usar o [BMP2Tile](http://www.smspower.org/maxim/Software/BMP2Tile).
Este programa pega uma gama de formatos de arquivo, como PNG, e os converte para os dados num formato que o Master 
System entende.

#### Quantos tiles nós podemos ter?

No formato mais típico de VRAM, 14KB do total de 16KB estão disponíveis para tiles; Isto é espaço o suficiente para 448 
tiles. (Com algumas mandingas, você consegue um espaço para alguns mais).

#### Não esqueça da paleta

Todos os seus dados de tile devem ser gerados com atenção para a paleta que serão aplicadas a eles. Os dados **não** 
contém as informações de paleta (ele trabalha só com índices de paleta). Tente usar um programa que ofereça um controle 
sobre a paleta, e não reordene-a!

#### Compressão

Os dados de tile pegam uma quantidade de  espaço relativamente grande - afinal, para preencher 14KB da VRAM serão 
necessários 14KB de dados. Contudo, eles são tipicamente comprimíveis. Há alguns esquemas de compressão disponíveis 
através do BMP2Tile, ou você pode fazer seu próprio.

#### Diferenças com o Game Gear

Não há diferença alguma - eles funcionam exatamente da mesma maneira.

### Mapa de tiles

Finalmente nós escrevemos o mapa de tiles. Isto diz para o VDP qual tile por em que localização da tela. Mesmo o 
resultado sendo  texto, o Master System não entende texto (ex: ASCII); nós temos que dizer os os números do tile. Ainda,
 ele ainda precisa de alguma informação extra sobre como exibir cada tile. Então a mensagem é armazenada num formato que
 o VDP entenda, e então a gente simplesmente copia os dados da ROM para a VRAM de volta. O endereço de destino na VRAM 
para o mapa de tiles é $3800. Nós iniciamos o valor novamente:

```asm
    ;==============================================================
    ; Escreve texto para a tabela de nomes
    ;==============================================================
    ; 1. Inicia o endereço de escrita da VRAM para o índice 0 do mapa de tiles
    ; escrevendo $4000 ORed com $3800+0
    ld a,$00
    out ($bf),a
    ld a,$38|$40
    out ($bf),a
```

Então nós copiamos os dados da ROM para a VRAM como antes:

```asm
    ; 2. Escreve dados do mapa de tiles
    ld hl,Message
    ld bc,MessageEnd-Message  ; Contador para o número de bytes a escrever
    WriteTextLoop:
        ld a,(hl)    ; Pega os bytes de dados
        out ($be),a
        inc hl       ; Aponta para a próxima letra
        dec bc
        ld a,b
        or c
        jp nz,WriteTextLoop
```

Agora os gráficos já estão completamente inicializados - todos os elementos necessários estão configurados na VRAM e na 
CRAM.

#### Detalhando o mapa de tiles

O mapa de tiles representa o plano preenchimento do plano de fundo da tela dos gráficos no Master System. Ele é 
ligeiramente mais largo do que a tela, permitindo que o ponto de vista deslize suavemente enqunato quaisquer atualizações
 ao mapa de tiles acontecem em partes fora da tela.
 
Considere a "tela virtual", que tem 256x244 pixels. Esta é construída a partir de uma grade de 32x28 tiles de 8x8. Cada 
entrada nesta grade de 32x28 é definida por uma entrada no mapa de tiles, algumas vezes chamada de *tabela de nomes*. 
Cada entrada determina:

- Qual tile exibir
- Qual paleta usar
- Sinalizadorees
    - Para fazer um tile ser desenhado invertido horizontalmente (como uma imagem espelhada)
    - Para fazer um tile ser desenhado invertido verticalmente (como um reflexo em um chão de espelhos)
    - Para fazer um tile ser desenhado na frente dos sprites
    
![Exemplo de mapa de tiles](../assets/lesson1/tilemap-example.png)

*Exemplo de tilemap - http://www.smspower.org/maxim/uploads/HowToProgram/tilemap-example.png*

#### Formato de dados

Os dados ocupam um total de 13 bits, armazenados em dois bytes:

| **Bit** | 15 - 13 | 12 | 11 | 10 | 9 | 8 - 0 |
|---------|---------|----|----|----|---|-------|
| **Dado** |Não utilizado| Prioridade | Paleta | inversão vertical | inversão horizontal | número do tile | 

Os dados são armazenados na VRAM no formato **little-endian**, i.e. os 8 últimos bits são armazenados primeiro.

O mapa de tiles normalmente é armazenado na VRAM no endereço $3800, e ocupa ae 1792 bytes (32z28x2 bytes).

#### Sinalizadores

##### Inversão

A inversão permite criar objetos simétricos com menos tiles, permitindo uma maior variedade de gráficos. No exemplo 
acima, alguns tiles foram invertidos horizontalmente, como mostrado pelas setas vermelhas na parte superior. 

##### Prioridade

Quando um tile tem o bit de prioridade configurado, todos os pixels com um índice maior do que 0 serão desenhados sobre 
os sprites. Você deve ent"ao escolher uma única cor na paleta para a posição 0 para ser a cor de plano de fundo 
para estes tiles, e eles terão um plano de fundo "transparente". Um uso cuidadoso do bit de prioridade pode causar uma 
impressão de gráficos em multi-camadas (plano de fundo, sprites, e primeiro plano).

![Bit de Prioridade](../assets/lesson1/tile-priority.png)

*Bit de prioridade - http://www.smspower.org/maxim/uploads/HowToProgram/tile-priority.png*

Repare como o sprite do monitor está escondido atrás da árvore, e o Sonic está escondido (discretamente) atrás da grama.
 Os tiles de primeiro plano estão destacados na imagem por um sombreado por razões de destaque. 
 
#### Sem tiless suficientes para preencher o mapa de tiles

Nós precisaríamos de aproximadamente 900 tile para preencher todo o mapa de tiles. Seriam necessários pelo menos 744 
para preencher a menor tela possível (no Master System). Só há espaço na VRAM para aproximadamente 450 tiles. No entanto, 
os gráficos devem ser construídos a partir dos tiles usando repetições do mesmo tile em mais de uma localização no mapa 
de tiles.

A falta de um display "bitmapeado" significa que você não pode fazer alguns tipos de gráfico, enquanto outros são muito 
difíceis de se conseguir.  Então a melhor maneira e projetar os seus gráficos é levar em conta esta limitação - desenhar
 linhas ou texto desalinhado é muito difícil, mas desenhar gráficos baseado em blocos é muito tranquilo. 

#### Conversão automática

[BMP2Tile](http://www.smspower.org/maxim/Software/BMP2Tile) gera o mapa de tiles para contemplar a imagem de entrada, 
embora seja sua responsabilidade tratar casos onde a imagem não seja do mesmo tamanho do próprio mapa de tiles. Usando
inversão de tile, você também pode reduzir o número de tiles necesários.

#### Diferenças com o Game Gear

Não há (quase) nenhuma.

A "tela virtual" é do mesmo tamanho, mas a parte exibida é, obviamente, a pequena tela do Game Gear de 160x144 no lugar 
da tela de 248x192 (tipicamente) do Master System. Isto significa que *há* tiles suficientes para encher toda a tela e 
fazer uma tela pseudo-bitmapeada (usando 360 tiles, deixando o bastante para sprites). Ainda é difícil trabalhar neste
modo, mas, como os dados não estão dispostos de uma maneira "bitmap" regular, os gráficos de tela cheia ainda são 
possíveis.

## Encerramento

Você deve estar pensando que temrinamos, agora que os gráficos foram configurados, porque tudo que há na tela é um Hello
 World. Na verdade, ainda precisamos de mais duas coisas:
- Ligar a tela

Ela está desligada desde que começamos - o dado de inicialização do VDP está desligado.

- Parar o programa

Se nós não o pararmos, ele continua seguindo, e coisas ruins vão acontecer.

### Ligando a tela - Registrador 1 do VDP

Os registradores do VDP controlam todos os aspectos da operação do VDP. O registrador 1 trata de ligar / desligar a tela.
 De fato, ele controla muitas coisas:
 
 | Bit |  Função | Se está configurado (1) | Se está desconfigurado (0) |
 |-----|---------|-------------------------|----------------------------|
 |  7  | Não utilizado | ----------------- | -------------------------- | 
 |  6  | Habilita tela | Tela ligada | Tela desligada |
 |  5  | Interrupção VBlank | Interruppões geradas no VBlank | VBlank não produz interrupções |
 |  4  | tela com 28 linhas | A tela é maior do que o normal (ex: jogos da Codemaster) | A tela é de tamanho normal |
 |  3  | tela com 30 linhas | A tela é ainda maior - mas apenas em sistemas PAL | A tela é de tamanho normal |
 |  2  | modo 5 do Mega Drive | Em um mega drive, o modo de vídeo do Mega Drive é ativado | Modo de vídeo do Master System |
 |  1  | Sprites duplicados | Cada sprite definido também exibe o próximo tile abaixo dele | Sprites normais | 
 |  0  | Sprites ampliados  | Os sprites são esticados para 16x16 pixels | Sprites normais |
 
A documentação "Sega Master System VDP Documentation", por Charles McDonald descreve todos os registradores com riqueza 
de detalhes. De qualquer forma, isto é muita informa informação para a gente lembrar toda vez, então faz sentido 
adicionarmos comentários para deixar claro o que estamos fazendo. Nós não queremos mexer com nenhuma destas características, 
exceto pela "Habilitar tela".

Para escrever no registrador do VDP, nós usamos a porta de controle do VDP. Desta vez, os dois bits mais altos devem ser 
%10. O resto é ligeiramente diferente já que não estamos configurando um endereço:    

| Bit: | 15 | 14 |  13 - 12  | 11 - 10 - 9 - 8 | 7 - 6 - 5 - 4 - 3 - 2 - 1 - 0 |
|------|----|----|-----------|-----------------|-------------------------------|
|  %   |  1 |  0 | Ignorado  |  Número do registrador  | Valor do Registrador  | 

Tudo é feito em 16 bits, então nós não precisamos usar a porta de dados do VDP. Nós precisamos escrever em um formato 
*little-endian* novamente, no entanto. Então, para configurar o registrador 1 para o aclot %01000000, nós fazemos isto:

```asm
    ; Liga a tela
    ld a,%01000000
;          ||||||`- Sprites ampliados -> 16x16 pixels
;          |||||`-- Sprites duplicados -> 2 tiles por sprite, 8x16
;          ||||`--- modo 5 do Mega Drive habilitado
;          |||`---- 30 linhas/ modo 240 linhas
;          ||`----- 28 linhas/ modo 224 linhas 
;          |`------ Interrupções VBlank 
;          `------- Habilita a tela
    out ($bf),a
    ld a,$81
    out ($bf),a
```

**Sugestão:** Use sempre um comentário como este toda vez que for escrever nos registradores do VDP, pois é muito 
difícil lembrar do que todos os bits fazem.

### Hora de parar - um laço de repetição infinito

OK, agora nosso código está quase terminado. Nós fizemos tudo que querpiamos fazer; mas ainda há mais uma coisa necessária.
O Z80 vai executar todo o código que escrevemos, e então ele vai até o fim, e isso seguie para sempre, sem parar. Nós não
queremos isto, nós queremos que o programa pare; então nós precisamos de um laço de repetição infinito. Normalmente, os
laços de repetição infinitos são uma coias ruim, pois eles interrompem o fluxo do seu programa de seguir em frente; você
provavelmente já criou alguns destes por acidente e teve que se desdobrar para descobrir o que acontecia e consertar o bug
introduzido causado por eles. Mas aqui, nós queremos um. Nós queremos que o processador siga fazendo a mesma coisa (nada)
para sempre, fazendo-o saltar para o próprio ponto de salto:

 ```adm
     ; Loop infinito para parar o programa
     Loop:
          jp Loop
``` 

Quando o Z80 lê a instrução `jp Loop` ele salta para o rótulo `Loop:`. Quando ele chega lá, ele encontra uma instrução 
`jp Loop` e salta para o rótulo `Loop:`. Quando ele chega lá, ele encontra uma instrução `jp Loop` e salta para o rótulo
 Loop:`... E assim segue.
 
Agora que adicionamos o loop infinito, nós queremos que o nosso programa jamais passe dele; então aqui é um lugar seguro
para por nossos dados. Por que é importante onde por os seus dados? Porque você deve ter certeza que seus dados não 
serão interpretados erroneamente como código. O Z80 não diferencia se o que está sendo interpretado é um dado ou uma 
linha de código sensível, então ele assume que tudo é código. Então você deve estar certo de que o lugar que você insere
seus dados é fora da área de código e que a execução jamais chegará na área dos seus dados. Para um programa simples como
esse (sem nenuhma "função", apenas um bloco de código), nós pusemos após o programa. Nós podíamos da mesma forma 
incluí-lo antes do rótulo "Main:", e no início do programa nós pularíamos isto e iríamos direto para este rótulo. 

Podemos inserir os dados na ordem que quisermos porque isto não importa - não é necessário inserir na ordem que serão 
usados. Em projetos maiores você pode escolher ordenar estes dados de maneira lógica para que a navegação faça mais 
sentido, e talvez dividir os dados de acordo com o que representam.

De qualquer forma, você deve ter reparado que i programa agora terminou de fato. Repita a execução para rodá-lo novamente.

# Turbinando seu programa

Há algumas coisas que podemos fazer com o programa para deixá-lo um pouco mais elegante.

## Definições no lugar de "números mágicos"

Na programação, há uma regra feral de que você não deve utilizar os chamados "números mágicos" = números que fazem algo
especial, mas que são apenas incluídos no meio do código de qualquer maneira. Em vez disto, você deve usar a sua linguagem
de programação para nomeá-los para que as pessoas possam ler o seu código e ver o significado, e não o valor:

```asm
;==============================================================
; Definições do Master System 
;==============================================================
.define VDPControl $bf
.define VDPData $be
.define VRAMWrite $4000
.define CRAMWrite $c000
``` 
`.define` é uma diretiva do WLA DX que define um noem para um determinado valor. Agora quando nós usarmos estes nomes 
(como "VDPControl"), o assembler vai agir como se tivéssemos inserido o valor ("$bf") em seu lugar. Assim, nós podemos 
usar estes no lugar dos números, por exemplo:

```asm
    ;==============================================================
    ; Configura os registradores do VDP
    ;==============================================================
    ld hl,VDPInitData
    ld b,VDPInitDataEnd-VDPInitData
    ld c,VDPControl
    otir
```   

## Funções auxiliares

Há algumas tarefas que executamos diversas vezes - por exemplo, configurar o endereço do VDP, e copiar dados para o VDP.
Nós podemos fazer funções que executam este código e chamar estas funções no lugar de repetir código.

No Z80, as funções normalmente sãointerpretadas usando instruções `call` e `ret`. Estas fazem uso da *pilha*. Vamos 
explicar a pilha primeiro.

### A pilha - empilhar, retirar, chamar, retornar

A descrição comum de pilha é que ela é como se fosse uma pihla de cartas, com a limitação mágica de que nós só podemos 
pegar a carta do topo da pilha, ou inserir outra carta nela. A coisa importante é que estas cartas saem da pilha na ordem
reversa sobre a qual entraram, então é importante não confundir isto.

Para o Z80, a pilha é a seção de memória contento *palavras* de 16 bits, não apenas bytes de 8 bits. Nós podemos empilhar
(**push**) um par de registradores na pilha e o Z80 vai armazená-lo nesta seção de memória. Nós então podemos retirá-lo 
(**pop**) de volta em qualquer par de registradores, embora geralmente só faça sentido retirá-lo no par de registradores
utilizado para empilhar. Isso permite você fazer algo como isto:

```asm
ld hl,$1234
push hl
     ld hl,$5678
     ; Faça alguma coisa com hl (que contém $5678)
     ; ...
pop hl
; hl agora contém $1234 novamente
``` 

...na prática, "salvando" o conteúdo daquele registrador para que você possa fazer outra coisa com ele, e então depois o
restaura ao seu estado anterior. O outro uso principal da pilha é para funções. Tem uma instrução do Z80 `call` que é 
exatamente como `jp`, e nisso faz com que a execução "pule" para um certo ponto no lugar de continuar linearmente; exceto
que primeiro ele **empilha** o par de registradores `pc` (contador de programa), que agora contém o endereço da próxima 
função após `call`, na pilha. Então, depois de algum tempo saltando para o endereço mencionado, se for encontrada uma 
chamada à isntrução **ret** ele vai **retirar** o endereço armazenado em `pc` e começa a executar o código que vem de lá,
com o efeito de **ret**ornar oara o ponto que ele estava anteriormente: 

Em outro lugar no programa, não no fluxo normal do programa:

```asm
MyFunction:
    inc a    ; Faça algo
    ret      ; retorne
```

No fluxo normal:

```asm
    ld a,$00
    call MyFunction
    ; a agora contém $01
    call MyFunction
    ; a agora contém $02
```

Novamente, você deve ser cuidadoso com a ordem que você empilha/retira, especialmente quando envolvido com `call`s e 
`ret`ornos. Veja, isto:

```asm
call MyFunction:
```

```asm
MyFunction:
    ld hl,$1234
    push hl
    ret        ; Erro!
```

não vai funcionar, porque a instrução `ret` pega a última coisa empilhada, que é $1234, e a execução continua em $1234! 
Exceto em circunstâncias bem específicas, isto não é algo desejado, porque $1234 pode ser algum dado, ou algum código 
nada relacionado, ou até mesmo o meio de alguma instrução!

Assim, para concluir, a pilha é uma área de memória que só podemos fazer `push` e `pop` (empilhar / desempilhar, 
respectivamente) para/de registradores; É também usada para `call` (chamar) funções e `ret`ornar delas; e nós temos de
ser cuidadosos com o *equilíbrio* do nosso uso da pilha para evitar de as coisas darem errado.

### Auxiliar 1: configurar o endereço VDP

Para configurar o endereço VDP, seja para a VRAM ou para a CRAM, nós queremos escrever no registradore de controle VDM, 
numa ordem *little-endian*.

```asm
SetVDPAddress:
; Configura o endereço VDP
; Parâmetros: hl = endereço
; Afeta: a
    ld a,l
    out (VDPControl),a
    ld a,h
    out (VDPControl),a
    ret
``` 

Isto é invocado usando o código:

``asm
    ; 1. Configura o acesso de escrita da VRAM para $0000
    ld hl,$0000 | VRAMWrite
    call SetVDPAddress
``

Os invocadores precisam de operar um OU no endereço com $4000 ou $c0000 dependendo de se estamos tratando de configurar 
o endereço de escrita da VRAM ou CRAM. "VRAMWrite" e "CRAMWrite" foram `.define` (definidas) anteriormente, para ajudar 
a deixar claro qual está sendo usada, como mostrado acima.

Repare que os comentários explicam detalhadamente o que a funçaõ faz, quais parâmetros são necessários, e quais 
registradores isto afeta. Desta maneira, ou usuários desta função podem tomar o cuidado de não deixar nada importante 
nestes registradores. Uma alternativa seria de `push`/`pop quaisquer registradores usados para evitar a perda de valores:

```asm
SetVDPAddress:
; Configura o endereço VDP
; Parâmetros: hl = endereço
    push af
        ld a,l
        out (VDPControl),a
        ld a,h
        out (VDPControl),a
    pop af
    ret
```

Eu usei identação para me ajudar a ter certeza de que meus `push`s e `pop`s estão equilibrados, e para mostrar o que é 
protegido por eles. No entanto, se o código invocador não se importar com o registrador `a`, esta proteção vai tornar o
programa mais lento.

### Auxiliar 2: Copiando dados para o VDP

```asm
CopyToVDP:
; Copia dados para o VDP
; Parâmetros: hl = endereço dos dados, bc = tamanho dos dados
; Afeta: a, hl, bc
-:  ld a,(hl)    ; Pega um byte de dados
    out (VDPData),a
    inc hl       ; Aponta para a próxima letra
    dec bc
    ld a,b
    or c
    jr nz,-
    ret
```

Isto é exatamente o que fizemos antes, com exceção de que nós empacotamos numa função, usando um rótulo anônimo e `jr`,
ambos serão explicados em breve.

A função pode ser invocada assim:

```asm
    ld hl,PaletteData
    ld bc,PaletteDataEnd-PaletteData
    call CopyToVDP
```

## Rótulos anônimos

Na versão original, nós temos diversos rótulos são basicamente usados só para laços de repetição. Nós tivemos que dar um
nome diferente para cada um, para que o WLA DX possa convertê-los depois; e uma vez que você tenha vários laços no seu 
programa, pensar em novos nomes se torna mais difícil. Já que não sãopartes necessariamente importantes do código, nós 
não precisamos de nomes que durem todo o programa; nós queremos nomes *temporários*. Uma maneira de fazer isto no WLA DX 
é usando os rótulos anônimos. Estes caem em três categorias:

| Tipo de rótulo | Parece com | Usado para |
|----------------|------------|------------|
|  Para frente   | Um ou mais sinais de "+"|  Lugares que você quer pular *para frente* |
|   Para trás    | Um ou mais sinais de "-"|  Lugares que você quer pular *para trás*   |
|   Mão dupla    | Dois *undersocres* "__" |  Um lugar que você quer pular *para frente* ou *para trás*, usando "_f" para pular pra frente, e "_b" para pular para trás. |
   

O detalhe especial sobre os rótulos anônimos pe que você pode reutilizá-los. se algum código quiser pular para o rótulo 
"-", o WLA DX vai encontrar a versão mais perto deste rótulo antes de pular, e usar esta. Assim, nossos quatro laços 
podem usar apenas um "-" no lugar de um rótulo completo.

## Salto relativo - `jr` 

Anteriormente, nós só usamos a instrução `jp` para realizar um salto. `jr` funciona (basicamente) da mesma maneira, 
exceto que é um salto relativo. Isto significa que no final do código, é armazenado como um número de bytes a saltar para
frente ou para trás, onde `jp` é armazenado como o endereço corrente para o salto. Há algumas vantafens e desvantagens:

- É um byte menor
- pode ser mais rápido para executar (porque é menor)
- Mas também pode ser mais lento para executar (porque o endereço tem que ser calculado)

A principal vantagem a se considerar é que te informa a você (que está lendo o código) quais saltos são para longe (i.e. 
longedo código anteriro, como uma seção diferente de código). Então eu (Maxim) pessoalmente sempre usro `jr` para saltos,
por exemplo, para ilustrar melhor que 'um salto para uma parte do bloco de código atual.' 

## Escrevendo texto no nosso arquivo

Antes, o mapa de tiles que desenha nosso teste era só uma massa de dados binários na ROM. Não seria mais maneiro se a 
gente pudesse apenas armazenar como texto? Tornaria bem mais fácil saber  o que seria escrito apenas olhando pro arquivo,
e muito mais fácil de mudar também. Para fazer isso, precisamos que aconteça uma série de coisas.

### Convertendo texto ASCII oara números de tile - `.asciitable`, `.asc` 

Usando a diretiva `.asciitable`, nós podemos dizer para o WLA DXcomo converter texto ASCII. Nossa fonte contém caracteres
desde o espaço (no tile número 0) até o '~' (no tile número $7e), na ordem normal ASCII (exceto para alguns caracteres 
especiais, como '£'). Nós podemos informar ao WLA DX sobre isso com:

```asm
.asciitable
map " " to "~" = 0
.enda
```

Então é só utilizar a diretiva `.asc` para armazenar texto, e o WLA DX vai convertê-lo para que os bytes remetam 
aos números de tile:

```asm
.asc "Hello world!
``` 

### Usando um valor sentinela para sinalizar o fim

Anteriormente, nós utilizamos rótulos para contar o tamanho do mapa de tiles. Há outra maneira, que é incluir um byte 
especial no fim do texto, que não corresponda a nenhuma outra letra. Quando este é encontrado, o código sabe que é a hora
de parar. Isto não funciona para dados genéricos, onde qualquer byte é válido, mas funciona para texto, já que nem todo
byte corresponde a um caracter.

Como nossa fonte utiliza os números de 0 até $7e, eu vou usar o valor $ff como meu sentinela ("terminador"):  

```asm
Message:
.asc "Hello world!
.db $ff
```

### Escrevendo todos os dados do mapa de tiles - formato do mapa de tiles, `cp`, sinalizadores, otimização `xor a`

Os dados do mapa de tiles não contém apenas números de tile com um byrte. Para começar, é possível ter mais de 256 tiles.
Junto a isto, há bits "sinalizadores", fazendo uma entrada ter 16 bits:

| **Bit** | 15 - 13 | 12 | 11  | 10 | 9 | 8 - 0 |
|---------|---------|----|-----|----|---|-------|
|**Uso**|não usado|Alta prioridade|usa a paleta de sprites|inverte verticalmente|inverter horizontalmente|número do tile|

Como todos os nossos tiles são de um número abaixo de 256, nós não precisamos usar nenhuma das funcionalidades mais 
avançadas, nós queremos apenas usar os 8 primeiros bits em 0 e o respot para ser o número de tiles.

Quando estamos escrevendo dados para o VDP, nós escrevemos eles em ordem *little-endian* como antes. Isto significa que 
nós vamos escrever primeiro o número do tile, e depois um 0.

Então o fluxo do programa fica:

1. Ler o byte
2. É $ff? Se sim, saia
3. Escreva o byte para o VDP
4. Escreva 0 para o VDP
5. Volte para 1.

Aqui está o código:


```asm
-:  ld a,(hl)
    cp $ff
    jr z,+
    out (VDPData),a
    xor a
    out (VDPData),a
    inc hl
    jr -
+:
```

Repare que para a saída, nós usamos o rótulo anônimo "+"; para a repetição, usamos o rótulo anônimo "-" (e `jr`).

Para checar se o valoe é $ff, nós utilizamos a instrução `cp`, de **c**om**p**aração. Esta configura alguns **sinalizadores**
baseado na comparação do registrador `a` e o parâmetro para a instrução (que pode ser um registrador ou um número literal),
Aqui vai uma versão simplificada dos efeitos dos sinalizadores:

| **Sinalizador**| ligado se | Condições relevantes |
|----------------|-----------|----------------------|
|      `z`       | **a** = valor |    `z`, `nz`     |
|      `c`       | **a** < valor |    `c`, `nc`     |

Internamente, funciona realizando uma subtração, armazenando o efeito sinalizador, mas descartando o resultado. Nós 
mencionamos os sinalizadores antes; Nís estamos novamente usando o sinalizador `z` (zero). Se subtrairmos $ff de um valor 
e a resposta for zero, então o valor deve ser $ff. Então nosso salto condicional será realizado, e o código do programa 
vai continuar com o que quer que venha depois do rótulo "+".

Sen ão é, então ele continua a escrever para a porta do VDP Como a instrução `cp` não armazena o resultado da subtração,
o registrador `a` ainda contém o valor que foi lido da ROM.

A próxima coisa que queremos escrever é um zero. Podemos fazer 
```asm
 ld a,$00
```
, mas é mais rápido e ocupa menos espaço na ROM fazer
```asm
 xor a
```

A instrução realiza um XOR entre o registrador `a` e o registrador ou número dado como parâmetro, e armazena o resultado 
no registrador `a`. XOR vai nos dar o binário 0 para cada bit que é o mesmo que o registrador `a` e o parâmetro, e o 
binário 1 para qualquer diferença entre eles. Já que o parâmetro é o próprio registrador `a`, ele avalia `a` XOR `a`, que 
vai sempre retornar um resultado 0. Ou, resumindo, `xor a` é um atalho eficiente para armazenar 0 no registrador `a`.

Finalmente, nós incrementamos `hl` para que se mova para o próximo número e repita até $ff ser encontrado.

## Arquivos de dados externos - `.incbin`

Você deve ter reparado que no Hello World original, mais da metade do tamanho do arquivo era tomado pelos dados binários
da fonte. Nós não queremos ler e modificar estes dados de qualquer forma (ele é pré-gerado automaticamente, não criado na
mão), então nós devemos movê-los para um arquivo externo. Há duas maneiras principais de incluir dados externos:

### `.include "nomedoarquivo"`

Isto funciona como se o conteúdo do arquivo mencionado fosse copiado e colado no arquivo atual. É bem parecido com o 
`#include` em C/C++. O arquivo deve ser então um texto que o WLA DX compreenda.

### `.incbin "nomedoarquivo"`

Este caso inclui os dados mencionados como dados brutos. Cada byte do arquivo é transferido como um byte para a ROM 
resultante. Nós usaremos esta opção para nossos dados de fonte. Eu converti os dados para dados bináriose salvei como 
"font.bin", então alterei o código para que parecesse com isso:

```asm
FontData:
.incbin "font.bin" fsize FontDataSize
``` 

O parâmetro "fsize" informa ao WLA DX que o que quero que ele assuma o valor de um símbolo chamado "FontDataSize", que 
corresponde ao tamanho do arquivo. Eu posso usar isso no lugar de "FontDataEnd-FontData" em qualquer lugar que eu queira
o tamanho dos dados em bytes.

## Versão final

[Esta versão](../assets/hello-world-2.asm) é esxatamente igual a [primeira versão](../assets/hello-world.asm), mas com 
todas as mudanças mencionadas acima. 
