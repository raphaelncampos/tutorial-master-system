# Maxim - Lesson 2

Veja em `http://www.smspower.org/maxim/HowToProgram/Lesson2`

Na lição 2, nós vamos construir um scroller de texto sobre a versão final do Hello World, uma introdução ao scrolling de
mapa de tiles. Nós também vamos aprender a usar o depurador.

## Mais boas práticas de código

### `.sections` - Seções

seções são uma maneira de permitir ao WLA DX que construa nosso arquivo de ROM para nós, e descarte tudo que não será 
utilizado. Isto é útil por diversas razões:

- Isto nos permite incluir dados / código extra no nosso código, e então descartar as partes que não serão usadas em 
tempo de compilação. Por exemplo
    - Nós podemos ter diversas fontes no nosso programa, e o WLA DX pode descartar as que não serão utilizadas.
    - Nós podemos ter um conjunto de funções úteis a vários projetos; E o WLA DX se encarrega de descartar qualquer 
código não utilizado.
- Permite ao WLA DX decidir onde por nossos código e dados automaticamente. Isto é particularmente útil quando criamos 
programas maiores que usam paginação.
- Nos permite especificar *alinhamento*, que pode ser bem útil para alguns truques de código.
- Como um bônus, nós podemos definir rótulos locais.

Se você está usando seções, então você deve garantir que **todo** o seu código as utilize, de outra forma você pode ter
problemas quando seu código ou dados são sobrescritos.

Você especifica seções assim:

```asm
.section "SomeFunction" free
SomeFunction:
  ; ...
  ret
.ends
``` 
Entre as diretivas `.section` e `.ends` temos apenas códigos e diretivas comuns, como antes. Nós especificamos o nome da
seção entre aspas duplas. Aqui, eu nomeei a seção com a funçãoque está contida nela; No entanto, nomes de seção quase
não têm restrições quanto ao número de caracteres que você usa, então você pode ser mais descritivo, se quiser.

A última palavra na linha é quase sempre um `free`. Isto diz ao WLA DX que nós não ligamos para o local que ele colocar 
o conteúdo da seção, contanto que esteja dentro do *banco* que especificamos. Nós voltaremos aos banco mais tade; por 
enquanto, nós vamos interpretar como dizer que este código pode ir em qualquer lugar.

Há alguns bits de código que devem ir em certos endereços particulares - o código de boot e os tratadores de interrupção 
(como Tratador de interrupção de pausa na [Lição 1](./Maxim - Lesson 1.md)). Para restes, no lugar de usar `free`, nós
escrevemos `force`.     

WLA DX percebe o uso de rótulos em cada seção. Se em qualquer lugar em nosso outro código tivermos
```asm
call SomeFunction
```
então o WLA DX saberá que nós estamos usando o conteúdos desta seção, então ele não descartará isto. Se não há esta 
referência, pode crer que ele *vai* descartar isso. Ele também lida com ao caso onde as únicas referências estão nas 
próprias seções e assim, as descarta. Em outras palavras, ele descarta tudo que não é necessário.

Referências são qualquer tipo de uso do rótulo - seja um `call`, `jp`, `jr`, `ld`, `dw`, ou qualquer outra coisa. (Repare
que `jr` para outra sessão diferente vai provavelmente quebrar, já que o WLA DX pode separar demais estas seções - como
dito, ele pode por em qualquer lugar).

Seções `force` jamais são descartadas. No entanto, a seção de inicialização (boot) é capaz de referenciar o código 
princial, e de lá as referências podem ser rastreadas como usadas.

Finalmente, há os **rótulos locais**. Estes são rótulos começando com um underscore (_). Eles são válidos somente dentro
da própria seção, o que significa que você pode usar os mesmos nomes para rótulso em outras seções sem encontrar nenuhm 
erro. Qualquer rótulo cujo uso não é esperado fora da seção pode tanto ser um rótulo anônimo quanto um rótulo local. Os
rótulos locais são naturalmente mais descritivos que os rótulos anônimos. 

## Usando o VBlank para contagem de tempo

### Porque eu preciso contar tempo?

Seu jogo vai precisar fazer algumas coisas com uma frequência - por exemplo, atualizar a tela, atualizar a música, ler 
as entradas do controle.  E vai precisar fazer isso:

- rapidamente, senão não será suave
- em intervalos regulares, senão as coisas irão se mover em velocidades diferentes e em tempos diferentes
- não tão rápido, senão não será percebido / se tornara injogável.

A interrupção VBlank satisfaz todas estas condições. Ela é disparada uma vez por quadro (então, para um sistema NTSC, 
será disparada 60 vezes por secunto, e em um sistema PAL, 50 vezes por segundo), o que é bem rápido o suficiente, mas 
não tão rápido assim, e é garantido em intervalos regulares.

E também tem a vantagem de que sinaliza o início do VBlank, que é um tempo bem especial...

### O que é o VBlank?

O VBlank é o período de tempo em que a tela não está sendo desenhada. Ele começa assim que a última linha é desenhada, e
termina logo depois que a primeira linha do próximo quadro é desenhada. Como a tela não está sendo desenhada neste tempo,
nós estamos livres para acessar a VRAM tão rapido quanto precisarmos, como se a tela estivesse desligada. Isto é de fato
muito importante, - porque queremos atualizar a tela, senão nosso programa será muito entediante.

Como isto é tão importante, tem uma maneira especial de tratar o VBlank no Master System: ele pode ser configurado para 
produzir uma **interrupção**.

### O que é uma interrupção?

[Lá na lição 1](./Maxim%20-%20Lesson%201.md), nós dissemos:

> Interrupções são coisas que podem causar  um salto da execução do código para outro lugar, "interrompendo" o fluxo do
> programa para tratar algo.

Elas permitem que o programa faça "duas coisas ao mesmo tempo". Pode ser executando um pedaço do código, e então algo 
dispara uma interrupção. A CPU então pausa o que está fazendo, e então vai executar algum trecho de código especial para
a interrupção, e depois recomeça de onde parou. Como essas coisas acontecem bem rápido, parece que elas estão acontecendo
ao mesmo tempo (embora não estejam, ok?).   

(De nada vale saber que as coisas funcionam desta forma se o código estiver escrito corretamente. Um tratamento de 
interrupções inadequado pode estragar tudo. Felizmente, nós não escreveremos um tratador de interrupções ruim.)

### Como eu uso as interrupções VBlank?

Há muitas maneiras diferentes de fazer isso. Para fins de simplicidad, só serácoberto um caso aqui: o alternável sistema 
faça-tudo-na interrupção. Infelizmente, é um pouco complicado - mas podia ser pior.

#### 1. Configure um tratador de interrupções

No Master System, nós só temos um tipo de interrupção (de hardware), comparado aos outros sistemas Z80. Nós só usamos o
**Interrupt Mode 1**, então todas as nossas interrupções são tratadas em `$0038`. Nós vamos usar apenas um tipo de 
interrupção (Interrupções VBlank - interrupções HBlanksão a única alternativa, e nós vamos deixar para outra aula).

Isto significa que nós temos que por algum código no deslocamente `$0038` para tratar as interrupções. Aqui vai:

```asm
.org $38
.section "Tratador de interrupcoes" force
  push af
    in a,(VDPStatus) ; satisfaz a interrupcao
    push hl
      ld hl,(VBlankRoutine)
      call CallHL
    pop hl
  pop af
  ei
  reti

CallHL:
  jp (hl)
.ends
```

Vamos um passo de cada vez, porque é bem complicado:

1. `.org $38` diz ao WLA DX que este código tem que ir no intervalo `$0038`.
2. `.section ...` permite que o código conviva pacificamente com outros códigos baseados em seção.
3. Nós `push/pop` (empilhamos/retiramos) todos os registradores que usamos. Isto é importante porque este tratador de 
interrupção pode estar interrompendo *qualquer trecho* de código no programa. Assim, temos que preservar o conteúdo
destes registradores já que não sabemos se são importantes ou não.
4. `in a, (VDPStatus)` *satisfaz a interrupção*. Isto permite ao VDP saber que nós tratamos a interrupção, o que significa
que o VDP vai produzir outra interrupção para o próximo VBlank. Se nõs não fizermos isto, as interrupções parariam.

    `VDPStatus` é um `.define`. Nós introduzimos este conceito em [Turbinando Seu Programa](./Maxim%20-%20Lesson%201.md), mas
    este é um novo. Em geral, você vai construir uma pequena biblioteca destas definições e usá-las quando necessário. Então,
    antes deste código, nós precisamos de:
    ```asm
    .define VDPStatus $bf
    ```
    Perceba que esta é a mesma definição `VDPData` usada anteriormente; Esta é somente leitura, e `VDPData` é somente escrita,
    portanto elas podem partilhar o mesmo número de porta. 

5. `ld hl,(VBlankRoutine)` busca um *ponteiro de função* na RAM. (`VBlankRoutine` deve ser definida na RAM em algum lugar
anterior a isto no código.)

6. `call CallHL` está fazendo um truque especial. Isto chama a função `CallHL`. Mas tudo que esta função faz é executar 
`jp (hl)`. (Alguns *disassemblers* podem chamar esta instrução de `ld pc,hl`. É a mesma instrução com uma sintaxe diferente.)
Isto significa que, contanto que `hl` contenha um endereço válido de função, esta função será executada. Quando a função
terminar de executar, ela deve `ret`ornar. Já que `CallHL` não `call` (chamou) a função, simplismente saltou para lá, isto
significa que este `ret`orno setá para o ponto logo após `call CallHL`.

7. Limpa os registradores empilhados...

8. A próxima é `ei`. Isto significa *enable interrupts* (permitir interrupções); Interrupções são automaticamente desabilitadas
quando elas acontecem, para permitir que o tratador de interrupções execute em ele próprio ser interrompido por mais interrupções
(!!!). Assim, nosso código tem que reabilitá-las. Você pode reabilitá-las dentro da interrupção ou fora dela; nós faremos
 isto dentro. Contudo, é importante reabilitá-las logo no fim, antes de...
 
9. `reti` retorna do tratador de interrupções e a execução continua do ponto que foi interrompida. É como se fosse o código
de operação `ret`, mas usamos `reti` porque este significa o fim tde um tratador de **i**nterrupção.

    (Nota técnica: `ei` é designado especialmente para habilitar as interrupções pouco tempo *depois* de chamado; de fato,
    apenas tempo suficiente para a execução de `reti`. Isto permite ao tratador de interrupções finalizar sua execução 
    completamente antes de as interrupções serem habilitadas. Se não fizéssemos isto, uma barreira de interrupções poderia
    superlotar o sistema, já que `reti` jamais executaria e então a pilha seria preenchida com endereços de retorno.)
    
Tudo isto é razoavelmente técnico e confuso. Eu escolhi pular para um tratador de interrupções mais avançado em vez de
simplificá-lo, porque tive que aprender com a experiência que, se você não faz as coisas coretamente desde o início, você
batalha com seus erros por um longo tempo... Então, confie em mim!

- Este tratador de interrupções funciona bem (para interrupções VBlank - interurpções HBlank são complicadores que não 
vamos inserir agora)
- Para utilizá-lo, você deve apenas configurar a `VBlankRoutine` para uma função pertinente.

#### 2. Ligando as interrupções VBlank.

Interrupções VBlank são *duplamente* habilitadas no Master System. Isto significa que:

- Você tem que habilitar as interrupções no VDP
- Você deve habilitar as interrupções na CPU

#### 3. Faça algo para o código não-VBlank fazer

- Use `halt`

### Programa de exemplo

- Desaparecimento da paleta?

# Trabalho em andamento

Seja paciente...

## Desenhando uma linha extra

Trabalho em andamento

## Introdução ao depurador 

Trabalho em andamento
